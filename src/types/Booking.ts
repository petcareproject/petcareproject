import type CageOrder from './CageOrder'
import type Customer from './Customer'
import type Pet from './Pet'
import type ServiceOrder from './ServiceOrder'

export default interface Booking {
  bgbId: number

  bgbBookingDate: string

  bgbBookingTime: string

  bgbStatus: string

  bgbDepositPrice: number

  bgbPetWeight: number

  bgbServicePrice: number

  bgbTotalPrice: number

  bgbDepPayStatus: string

  bgbDepPayMethod: string

  bgbTotalPayStatus: string

  bgbTotalPayMethod: string

  customer: Customer

  serviceOrders: ServiceOrder[]

  cageOrderId: CageOrder

  pet: Pet

  show?: boolean
}
