export default interface Customer {
  cusId?: number

  cusUsername: string

  cusPassword: string

  cusEmail: string

  cusName: string

  cusSurname: string

  cusPhone: string

  createdDate?: Date

  updatedDate?: Date
}
