export default interface UserLogin {
  id: number
  email: string
  username: string
  role: string
  pic: string
}
