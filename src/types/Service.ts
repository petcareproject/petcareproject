import type ServiceOrder from './ServiceOrder'

export default interface Service {
  serviceId: number

  serviceName: string

  createdDate?: Date

  updatedDate?: Date

  serviceOrder: ServiceOrder[]
}
