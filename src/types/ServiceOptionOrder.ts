import type Option from './Option'
import type ServiceOrder from './ServiceOrder'

export default interface ServiceOptionOrder {
  serviceOptionOrderId?: number

  optionName: string

  optionPrice?: number

  option: Option

  serviceOrder: ServiceOrder
}
