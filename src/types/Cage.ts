export default interface Cage {
  cageId?: number

  cageStatus: String

  createdDate?: Date

  updatedDate?: Date

  roomId?: number
}