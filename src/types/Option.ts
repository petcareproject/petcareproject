import type ServiceOptionOrder from './ServiceOptionOrder'

export default interface Option {
  optionId: number

  optionName: string

  createdDate: Date

  updatedDate: Date

  serviceId: number

  serviceOptionOrders: ServiceOptionOrder[]
}
