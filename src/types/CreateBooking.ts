export default interface CreateBooking {
  bgbBookingDate: String | null
  bgbBookingTime: string
  bgbStatus: string
  bgbPetWeight: number
  bgbServicePrice: number
  bgbTotalPrice: number
  bgbDepPayStatus: string
  bgbDepPayMethod: string
  bgbTotalPayStatus: string
  bgbTotalPayMethod: string
  cusId: number
  petId: number
  serviceOrders: {
    serviceId: number
    serviceName: string
    note: string
    serviceOptionOrders: {
      optionId: number
      optionName: string
    }[]
  }[]
}
