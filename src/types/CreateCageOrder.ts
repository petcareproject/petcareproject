export default interface CreateCageOrder {
  cageOrderStatus: String
  cageId: number
  bgbookingId: number
  roomId: number
  petId: number
}
