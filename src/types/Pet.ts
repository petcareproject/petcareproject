export default interface Pet {
  petId: number

  petPicture: string

  petName: string

  petType: string

  petBreed: string

  petBirthday: Date

  petAge: string

  createdDate: Date

  updatedDate: Date

  cusId: number
}
