import type Booking from './Booking'
import type Service from './Service'
import type ServiceOptionOrder from './ServiceOptionOrder'

export default interface ServiceOrder {
  serviceOrderId?: number

  serviceName: string

  totalPrice?: number

  note?: string

  service: Service

  bgbooking: Booking

  serviceOptionOrders: ServiceOptionOrder[]
}
