import type Booking from './Booking'
import type Cage from './Cage'
import type Pet from './Pet'
import type Room from './Room'

export default interface CageOrder {
  cageOrderId: number

  cageOrderStatus: string

  cage: Cage

  bgbooking: Booking

  room: Room

  pet: Pet

  createdDate: Date

  updatedDate: Date
}
