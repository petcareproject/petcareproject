import http from './axios'
function getService() {
  return http.get('/services')
}

export default { getService }
