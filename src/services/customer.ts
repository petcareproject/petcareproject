import type Customer from '@/types/Customer'
import http from './axios'
function addCustomer(customer: Customer) {
  return http.post('/customer', customer)
}

function updateCustomer(cusId: number, customer: Customer) {
  return http.post(`/customer/${cusId}`, customer)
}

function getCustomer() {
  return http.get('/customer')
}

function getCustomerById(cusId: number) {
  return http.get(`/customer/${cusId}`)
}

export default { addCustomer, getCustomer, updateCustomer, getCustomerById }
