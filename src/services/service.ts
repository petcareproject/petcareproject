import http from './axios'
function getServices() {
  return http.get('/services')
}
function getServiceById(id: number) {
  return http.get(`/services/${id}`)
}
export default { getServices, getServiceById }
