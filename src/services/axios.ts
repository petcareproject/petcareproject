// axios.js
import axios from 'axios'
import router from '@/router'

const instance = axios.create({
  baseURL: 'http://localhost:3000'
})

instance.interceptors.request.use(
  async function (config) {
    const token = localStorage.getItem('token')
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
  },
  function (error) {
    return Promise.reject(error)
  }
)

instance.interceptors.response.use(
  function (response) {
    return response
  },
  function (error) {
    if (error.response && error.response.status === 401) {
      router.replace('/login')
    }
    return Promise.reject(error)
  }
)

export default instance
