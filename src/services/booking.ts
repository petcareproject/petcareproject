import type Booking from '@/types/Booking'
import http from './axios'
import type CreateBooking from '@/types/CreateBooking'
function addBgBooking(booking: CreateBooking) {
  return http.post('/bgbooking', booking)
}

function getBgBooking(params: { search: string }) {
  return http.get('/bgbooking', { params: params })
}

function getBgBookingByCustomer(id: number) {
  return http.get(`/bgbooking/customer/${id}`)
}

function deleteBookingNoCage(id: number) {
  return http.delete(`/bgbooking/${id}`)
}
function updateBgBooking(bgbId: number, booking: Booking) {
  return http.patch(`/bgbooking/${bgbId}`, booking)
}

export default {
  addBgBooking,
  getBgBooking,
  updateBgBooking,
  getBgBookingByCustomer,
  deleteBookingNoCage
}
