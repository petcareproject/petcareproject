import http from './axios'
function getOptions() {
  return http.get('/options')
}
function getOptionByServiceId1(id: number) {
  return http.get(`/options/service/${id}`)
}
function getOptionByServiceId2(id: number) {
  return http.get(`/options/service/${id}`)
}

export default { getOptions, getOptionByServiceId1, getOptionByServiceId2 }
