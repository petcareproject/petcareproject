import type ServiceOrder from '@/types/ServiceOrder'
import http from './axios'
function getServiceOrder() {
  return http.get('/serviceorder')
}

function addServiceOrder(serviceOrder: ServiceOrder) {
  return http.post('/serviceorder', serviceOrder)
}

function updateServiceOrder(serviceOrderId: number, serviceOrder: ServiceOrder) {
  return http.patch(`/serviceorder/${serviceOrderId}`, serviceOrder)
}
function getServiceByBooking(id: number) {
  return http.get(`/serviceorder/bgbooking/${id}`)
}
export default { getServiceOrder, getServiceByBooking ,updateServiceOrder}
