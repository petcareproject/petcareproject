import type Pet from '@/types/Pet'
import http from './axios'
function getPets() {
  return http.get('/pets')
}

function getPetByCustomer(id: number) {
  return http.get(`/pets/customer/${id}`)
}

function getPetByImageString(imageFile: String) {
  return http.get(`/pets/image/${imageFile}`)
}

function savePet(pet: Pet) {
  return http.post('/pets', pet)
}
function updatePet(id: number, pet: Pet) {
  return http.patch(`/pets/${id}`, pet)
}
function deletePet(id: number) {
  return http.delete(`/pets/${id}`)
}
export default { getPets, savePet, updatePet, deletePet, getPetByCustomer, getPetByImageString }
