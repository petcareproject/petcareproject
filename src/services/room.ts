import type Room from '@/types/Room'
import http from './axios'
function addRoom(room: Room) {
  return http.post('/rooms', room)
}
function getRoom() {
  return http.get('/rooms')
}

export default { addRoom, getRoom }