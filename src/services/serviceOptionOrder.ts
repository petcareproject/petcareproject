import type ServiceOptionOrder from '@/types/ServiceOptionOrder'
import http from './axios'
function getServiceOptionOrder() {
  return http.get('/serviceoptionorders')
}

function addServiceOptionOrder(serviceOptionOrder: ServiceOptionOrder) {
  return http.post('/serviceoptionorders', serviceOptionOrder)
}

function updateServiceOptionOrder(serviceOptionId: number, serviceOptionOrder: ServiceOptionOrder) {
  return http.patch(`/serviceoptionorders/${serviceOptionId}`, serviceOptionOrder)
}

export default { getServiceOptionOrder, addServiceOptionOrder, updateServiceOptionOrder }
