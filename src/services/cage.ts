import type Cage from '@/types/Cage'
import http from './axios'
function addCage(cage: Cage) {
  return http.post('/cages', cage)
}
function getCage() {
  return http.get('/cages')
}

function getCageById(id: number) {
  return http.get(`/cages/room/${id}`)
}
export default { addCage, getCage, getCageById }
