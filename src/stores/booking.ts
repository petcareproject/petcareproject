import type Booking from '@/types/Booking'
import { defineStore } from 'pinia'
import { computed, onMounted, ref, watch } from 'vue'
import bookingService from '@/services/booking'
import type ServiceOrder from '@/types/ServiceOrder'
import type CreateBooking from '@/types/CreateBooking'
import { usePetStore } from './pet'
import { useServiceStore } from '@/stores/service'
import { useOptioneStore } from '@/stores/option'
import { useAuthStore } from './auth'
import { useMessageStore } from './message'
import router from '@/router'

export const useBookingStore = defineStore('Booking', () => {
  const booking = ref<Booking[]>([])
  const bookingbycus = ref<Booking[]>([])
  const selectedbath = ref(false)
  const bathoption = ref([])
  const notebath = ref('')
  const selectedgroom = ref(false)
  const groomoption = ref()
  const notegroom = ref('')
  const selectedtime = ref('')
  const selecteddate = ref<Date | null>(null)
  const selectedDMethod = ref('')
  const backendURL = 'http://localhost:3000'
  const selectedBathServices = ref([])
  const selectedGroomServices = ref([])
  const petStore = usePetStore()
  const serviceStore = useServiceStore()
  const optionStore = useOptioneStore()
  const authStore = useAuthStore()
  const messageStore = useMessageStore()
  const totalServicePrice = ref<number>(0)
  const petWeight = ref<number>(0)

  const bathServices = computed(() => {
    return serviceStore.service.filter((service) => service.serviceId === 1)
  })

  const groomServices = computed(() => {
    return serviceStore.service.filter((service) => service.serviceId === 2)
  })

  onMounted(async () => {
    await serviceStore.getService()
    await optionStore.getOption()
  })

  //create booking
  const bgBooking = ref<CreateBooking>({
    bgbBookingDate: null,
    bgbBookingTime: '',
    bgbStatus: 'รอเข้ารับบริการ',
    bgbPetWeight: 0,
    bgbServicePrice: 0,
    bgbTotalPrice: 0,
    bgbDepPayStatus: '',
    bgbDepPayMethod: '',
    bgbTotalPayStatus: 'ยังไม่ชำระ',
    bgbTotalPayMethod: '',
    cusId: 0,
    petId: 0,
    serviceOrders: []
  })

  const editedbgBooking = ref<Booking>()

  function clearBooking() {
    bgBooking.value = {
      bgbBookingDate: null,
      bgbBookingTime: '',
      bgbStatus: 'รอเข้ารับบริการ',
      bgbPetWeight: 0,
      bgbServicePrice: 0,
      bgbTotalPrice: 0,
      bgbDepPayStatus: '',
      bgbDepPayMethod: '',
      bgbTotalPayStatus: 'ยังไม่ชำระ',
      bgbTotalPayMethod: '',
      cusId: 0,
      petId: 0,
      serviceOrders: []
    }
  }

  watch(selecteddate, () => {
    const formattedDate = `${selecteddate.value?.getDate()}/${
      selecteddate.value!.getMonth() + 1
    }/${selecteddate.value!.getFullYear()}`
    bgBooking.value.bgbBookingDate = formattedDate
    console.log('Selected date:', bgBooking.value.bgbBookingDate)
  })

  watch(bathServices, () => {
    if (!bathServices.value) {
      bathoption.value = []
      notebath.value = ''
    }
  })
  watch(groomServices, () => {
    if (!groomServices.value) {
      groomoption.value = undefined
      notegroom.value = ''
    }
  })

  watch(bgBooking.value, () => {
    console.log(bgBooking.value)
  })

  watch(notebath, (newValue) => {
    const bathService = bgBooking.value.serviceOrders.find((service) => service.serviceId === 1)
    if (bathService) {
      bathService.note = newValue
    }
  })

  watch(notegroom, (newValue) => {
    const groomService = bgBooking.value.serviceOrders.find((service) => service.serviceId === 2)
    if (groomService) {
      groomService.note = newValue
    }
  })

  watch(
    () => petStore.selectedPet,
    () => {
      if (petStore.selectedPet) {
        bgBooking.value.petId = petStore.selectedPet.petId
      }
    }
  )

  function handleButtonClick(time: string): void {
    selectedtime.value = time
    console.log(selectedtime)
  }

  async function getBooking(search: string) {
    try {
      const res = await bookingService.getBgBooking({ search: search })
      booking.value = res.data
      console.log('ดึงข้อมูลบุ๊กกิ้งสำเร็จ จริงปะ งง')
    } catch (e) {
      console.log(e)
    }
  }

  async function getBookingByCus() {
    const currentCustomer = authStore.getCurrentCustomer()
    if (currentCustomer && currentCustomer.id !== undefined) {
      const cusId = currentCustomer.id
      try {
        const res = await bookingService.getBgBookingByCustomer(cusId)
        bookingbycus.value = res.data
        console.log('ดึงข้อมูลบุ๊กกิ้งสำเร็จ', bookingbycus.value)
      } catch (error) {
        console.error(error)
      }
    } else {
      console.error('User ID is undefined')
    }
  }

  async function addBooking() {
    try {
      await bookingService.addBgBooking(bgBooking.value)
      console.log('booking success')
      clearBooking()
      router.push({ path: '/paymentSuccess' })
    } catch (e) {
      console.log('booking not save : ', e)
    }
  }

  async function delBookingNoCage(id: number) {
    try {
      await bookingService.deleteBookingNoCage(id)
      console.log('Delete booking success')
      clearBooking()
      getBookingByCus()
    } catch (e) {
      console.log('Delete booking not success : ', e)
    }
    window.location.reload
  }

  async function updateBooking(booking: Booking) {
    editedbgBooking.value = booking
    console.log(editedbgBooking.value)
    router.push('/caltotalprice')
  }
  async function updateBookingPayment(booking: Booking) {
    editedbgBooking.value = booking
    console.log(editedbgBooking.value)
    router.push('/paymentemp')
  }

  function setTotalServicePrice(price: number, weight: number) {
    totalServicePrice.value = price
    petWeight.value = weight
  }

  async function updateBookingBackend(updatedBooking: Booking) {
    if (editedbgBooking.value) {
      console.log('Before update:', editedbgBooking.value)
      editedbgBooking.value.bgbServicePrice = totalServicePrice.value
      editedbgBooking.value.bgbPetWeight = petWeight.value
      editedbgBooking.value.bgbTotalPrice =
        editedbgBooking.value.bgbServicePrice + editedbgBooking.value.bgbDepositPrice
      editedbgBooking.value.bgbTotalPayMethod
      editedbgBooking.value.bgbTotalPayStatus
      console.log('After update:', editedbgBooking.value)
      try {
        await bookingService.updateBgBooking(editedbgBooking.value.bgbId, updatedBooking)
        console.log('Booking updated successfully')
      } catch (error) {
        console.error('Error updating booking:', error)
      }
    } else {
      console.error('editedbgBooking.value is undefined')
    }
  }

  return {
    selectedbath,
    bathoption,
    notebath,
    selectedgroom,
    selecteddate,
    backendURL,
    handleButtonClick,
    groomoption,
    selectedtime,
    notegroom,
    selectedDMethod,
    getBooking,
    booking,
    bgBooking,
    selectedBathServices,
    selectedGroomServices,
    getBookingByCus,
    bookingbycus,
    addBooking,
    clearBooking,
    delBookingNoCage,
    updateBooking,
    editedbgBooking,
    totalServicePrice,
    setTotalServicePrice,
    updateBookingBackend,
    petWeight,
    updateBookingPayment
  }
})
