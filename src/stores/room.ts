import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import roomService from '@/services/room'
import cageService from '@/services/cage'
import type Room from '@/types/Room'
import type Cage from '@/types/Cage'

export const useRoomStore = defineStore('Room', () => {
  const showRoomCage = ref(false)
  const room = ref<Room[]>([])
  const cage = ref<Cage[]>([])
  const roomIds = computed(() => room.value.map((room) => room.roomId))
  async function openRoomCage() {
    showRoomCage.value = true
  }
  async function getRoom() {
    try {
      const res = await roomService.getRoom()
      room.value = res.data
    } catch (e) {
      console.log(e)
    }
  }
  async function getCageById(id: number) {
    try {
      const res = await cageService.getCageById(id)
      cage.value = res.data
    } catch (e) {
      console.log(e)
    }
  }

  return { showRoomCage, openRoomCage, getRoom, room, roomIds, getCageById, cage }
})
