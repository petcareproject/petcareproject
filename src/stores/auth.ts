import { defineStore } from 'pinia'
import { useMessageStore } from './message'
import router from '@/router'
import authService from '@/services/auth'
import { ref } from 'vue'
import type UserLogin from '@/types/userLogin'

export const useAuthStore = defineStore('auth', () => {
  const messageStore = useMessageStore()
  const email = ref('')
  const password = ref('')

  function getCurrentCustomer(): UserLogin | null {
    const strUser = localStorage.getItem('customerData') || null
    return strUser ? JSON.parse(strUser) : null
  }
  function getCurrentUser(): UserLogin | null {
    const strUser = localStorage.getItem('userData') || null
    return strUser ? JSON.parse(strUser) : null
  }

  function getToken(): UserLogin | null {
    const strToken = localStorage.getItem('access_token')!
    return JSON.parse(strToken)
  }
  const login = async (): Promise<void> => {
    try {
      const res = await authService.login(email.value, password.value)
      const userData = res.data.user
      const customerData = res.data.customer

      if (userData) {
        localStorage.setItem('userData', JSON.stringify(userData))
        localStorage.setItem('access_token', JSON.stringify(userData.access_token))
        localStorage.setItem('id', userData.id)
        localStorage.setItem('email', userData.email)
        localStorage.setItem('username', userData.username)
        localStorage.setItem('role', userData.role)
        localStorage.setItem('pic', userData.pic)
        router.push('/employeemain')
      }

      if (customerData) {
        localStorage.setItem('customerData', JSON.stringify(customerData))
        localStorage.setItem('access_token', JSON.stringify(customerData.access_token))
        localStorage.setItem('id', customerData.id)
        localStorage.setItem('email', customerData.email)
        localStorage.setItem('username', customerData.username)
        localStorage.setItem('role', 'ลูกค้า')
        localStorage.setItem('pic', customerData.pic)
        router.push('/')
      }

      messageStore.showConfirm('เข้าสู่ระบบเรียบร้อย')
    } catch (e) {
      console.error(e)
      messageStore.showError('Username หรือ Password ไม่ถูกต้อง')
    }
  }

  const logout = () => {
    localStorage.removeItem('userData')
    localStorage.removeItem('customerData')
    localStorage.removeItem('access_token')
    localStorage.removeItem('id')
    localStorage.removeItem('email')
    localStorage.removeItem('username')
    localStorage.removeItem('role')
    localStorage.removeItem('pic')
    router.replace('/login')
  }

  return {
    login,
    email,
    password,
    getCurrentUser,
    getToken,
    logout,
    getCurrentCustomer
  }
})
