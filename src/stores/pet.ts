import { defineStore } from 'pinia'
import { ref } from 'vue'
import petService from '@/services/pet'
import type Pet from '@/types/Pet'
import { useAuthStore } from './auth'
import { useMessageStore } from './message'

export const usePetStore = defineStore('Pet', () => {
  const pets = ref<Pet[]>([])
  const selectedPet = ref<Pet | null>(null)
  const imageSelectedPet = ref()

  const showAddPet = ref(false)
  const showAddDataPet = ref(false)
  const showServicePrice = ref(false)
  const authStore = useAuthStore()
  const messageStore = useMessageStore()

  async function openAddPet() {
    selectedPet.value = null
    showAddPet.value = true
  }
  async function addPetDialog() {
    showAddDataPet.value = true
  }

  async function selectPet(pet: Pet) {
    selectedPet.value = pet
    imageSelectedPet.value = pet.petPicture
    showAddPet.value = false
  }

  async function getPets() {
    const currentCustomer = authStore.getCurrentCustomer()
    if (currentCustomer && currentCustomer.id !== undefined) {
      const cusId = currentCustomer.id
      try {
        const res = await petService.getPetByCustomer(cusId)
        pets.value = res.data
      } catch (error) {
        console.error(error)
      }
    } else {
      console.error('User ID is undefined')
    }
  }

  async function openServicePrice() {
    showServicePrice.value = true
  }

  return {
    pets,
    selectedPet,
    showAddPet,
    openAddPet,
    selectPet,
    addPetDialog,
    showAddDataPet,
    getPets,
    showServicePrice,
    openServicePrice,
    imageSelectedPet
  }
})
