import serviceOrder from '@/services/serviceOrder'
import type ServiceOrder from '@/types/ServiceOrder'
import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useServiceOrderStore = defineStore('ServiceOrder', () => {
  const service = ref<ServiceOrder[]>([])
  const serviceorder = ref<ServiceOrder[]>([])
  async function getServiceByBookingId(serviceId: number) {
    try {
      const res = await serviceOrder.getServiceByBooking(serviceId)
      service.value = res.data
      return res.data
    } catch (e) {
      console.log(e)
      return []
    }
  }
  async function getServiceOrder() {
    try {
      const res = await serviceOrder.getServiceOrder()
      serviceorder.value = res.data
    } catch (e) {
      console.log('error: ', e)
    }
  }
  return { getServiceByBookingId, getServiceOrder, serviceorder, service }
})
