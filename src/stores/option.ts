import type Option from '@/types/Option'
import { defineStore } from 'pinia'
import { ref } from 'vue'
import optionService from '@/services/option'

export const useOptioneStore = defineStore('Option', () => {
  const option = ref<Option[]>([])
  const option1 = ref<Option[]>([])
  const option2 = ref<Option[]>([])

  async function getOption() {
    try {
      const res = await optionService.getOptions()
      res.data = option.value
    } catch (e) {
      console.log('error: ', e)
    }
  }
  async function getOptionByServiceId1(serviceId: number) {
    try {
      const res = await optionService.getOptionByServiceId1(serviceId)
      option1.value = res.data
      return res.data
    } catch (e) {
      console.log(e)
      return []
    }
  }
  async function getOptionByServiceId2(serviceId: number) {
    try {
      const res = await optionService.getOptionByServiceId2(serviceId)
      option2.value = res.data
      return res.data
    } catch (e) {
      console.log(e)
      return []
    }
  }

  return {
    option,
    option1,
    option2,
    getOption,
    getOptionByServiceId1,
    getOptionByServiceId2
  }
})
