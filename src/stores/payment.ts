import { defineStore } from 'pinia'
import { ref } from 'vue'

export const usePaymentStore = defineStore('Payment', () => {
  const stepPayments = [[0], [0, 1], [0, 1, 2]]

  return {
    stepPayments
  }
})
