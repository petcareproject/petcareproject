import type Service from '@/types/Service'
import { defineStore } from 'pinia'
import { ref } from 'vue'
import bgserviceService from '@/services/bgservice'
import serviceService from '@/services/service'

export const useServiceStore = defineStore('Service', () => {
  const service = ref<Service[]>([])

  async function getService() {
    try {
      const res = await serviceService.getServices()
      service.value = res.data
    } catch (error) {
      console.error('Error fetching services:', error)
    }
  }
  async function getServiceById(id: number) {
    try {
      const res = await serviceService.getServiceById(id)
      service.value = [res.data]
    } catch (error) {
      console.error('Error fetching services:', error)
    }
  }
  return {
    service,
    getService,
    getServiceById
  }
})
