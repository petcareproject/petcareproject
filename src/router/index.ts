import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'main',

      components: {
        default: () => import('../views/MainView.vue')
      }
    },
    {
      path: '/login',
      name: 'login',

      components: {
        default: () => import('../views/LoginView.vue')
      },

      meta: {
        login: true
      }
    },
    {
      path: '/success',
      name: 'Success',

      components: {
        default: () => import('../views/SuccessView.vue')
      },

      meta: {
        login: true
      }
    },
    {
      path: '/Singin',
      name: 'Singin',

      components: {
        default: () => import('../views/SinginView.vue')
      },

      meta: {
        login: true
      }
    },
    {
      path: '/bathroom',
      name: 'bathroom',

      components: {
        default: () => import('../views/BathroomView.vue')
      }
    },
    {
      path: '/farkliang',
      name: 'farkliang',

      components: {
        default: () => import('../views/FarkliangView.vue')
      }
    },
    {
      path: '/Buycourse',
      name: 'Buycourse',

      components: {
        default: () => import('../views/BuycourseView.vue')
      }
    },
    {
      path: '/Coursetime',
      name: 'Coursetime',

      components: {
        default: () => import('../views/CoursetimebookingView.vue')
      }
    },
    {
      path: '/manage_customer',
      name: 'Manage_customer',

      components: {
        default: () => import('../views/CustomerUserView.vue')
      }
    },
    {
      path: '/cart',
      name: 'Cart',

      components: {
        default: () => import('../views/CartView.vue')
      }
    },
    {
      path: '/booking',
      name: 'Booking',

      components: {
        default: () => import('../views/BookingView.vue')
      }
    },
    {
      path: '/payment',
      name: 'Payment',

      components: {
        default: () => import('../views/PayMentView.vue')
      }
    },
    {
      path: '/qrcode',
      name: 'Qrcode',

      components: {
        default: () => import('../views/qrcodeView.vue')
      }
    },
    {
      path: '/paymentSuccess',
      name: 'PaymentSuccess',

      components: {
        default: () => import('../views/PaymentSuccessView.vue')
      }
    },
    {
      path: '/credit',
      name: 'Credit',

      components: {
        default: () => import('../views/CreditView.vue')
      }
    },
    {
      path: '/course1',
      name: 'Course1',

      components: {
        default: () => import('../views/FirstCourseView.vue')
      }
    },
    {
      path: '/course2',
      name: 'Course2',

      components: {
        default: () => import('../views/SecondCourseView.vue')
      }
    },
    {
      path: '/course3',
      name: 'Course3',

      components: {
        default: () => import('../views/ThreeCourseView.vue')
      }
    },
    {
      path: '/employeemain',
      name: 'EmployeeMain',

      components: {
        default: () => import('../views/Employee/EmployeeMainView.vue')
      },

      meta: {
        employee: true
      }
    },
    {
      path: '/employeecourse',
      name: 'EmployeeCourse',

      components: {
        default: () => import('../views/Employee/EmployeeCourseView.vue')
      },
      meta: {
        employee: true
      }
    },
    {
      path: '/employeebathroom',
      name: 'EmployeeBathRoom',

      components: {
        default: () => import('../views/Employee/EmployeeBathroomView.vue')
      },
      meta: {
        employee: true
      }
    },
    {
      path: '/employeehotelpet',
      name: 'EmployeeHotelPet',

      components: {
        default: () => import('../views/Employee/EmployeeHotelPetView.vue')
      },
      meta: {
        employee: true
      }
    },
    {
      path: '/customerlist',
      name: 'CustomerList',

      components: {
        default: () => import('../views/Employee/CustomerListView.vue')
      },
      meta: {
        employee: true
      }
    },
    {
      path: '/manage_user',
      name: 'Manage_user',

      components: {
        default: () => import('../views/Employee/ManageUserView.vue')
      },
      meta: {
        employee: true
      }
    },
    {
      path: '/caltotalprice',
      name: 'CalTotalPrice',

      components: {
        default: () => import('../views/Employee/CalTotalPriceView.vue')
      },
      meta: {
        employee: true
      }
    },
    {
      path: '/paymentemp',
      name: 'PayMentEmp',

      components: {
        default: () => import('../views/Employee/PayMentEmpView.vue')
      },
      meta: {
        employee: true
      }
    },
    {
      path: '/qrcodeemp',
      name: 'QrCodeEmp',

      components: {
        default: () => import('../views/Employee/qrcodeEmpView.vue')
      },
      meta: {
        employee: true
      }
    },
    {
      path: '/paysuccessemp',
      name: 'PaySuccessEmp',

      components: {
        default: () => import('../views/Employee/PaymentSuccessEmpView.vue')
      },
      meta: {
        employee: true
      }
    },
    {
      path: '/creditemp',
      name: 'CreditEmp',

      components: {
        default: () => import('../views/Employee/CreditEmpView.vue')
      },
      meta: {
        employee: true
      }
    }
  ]
})
// function isLogin() {
//   const user = localStorage.getItem("user");
//   if (user) {
//     return true;
//   }
//   return false;
// }

// router.beforeEach((to, from) => {
//   if (to.meta.requiresAuth && !isLogin()) {
//     return {
//       path: "/",
//       query: { redirect: to.fullPath },
//     };
//   }
// });
export default router
